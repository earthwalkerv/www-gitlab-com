---
layout: handbook-page-toc
title: "Open Source Program"
description: Learn about the GitLab for Open Source Program and other open source programs from GitLab's Community Relations team.
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# <i class="far fa-newspaper" id="biz-tech-icons"></i>  About
GitLab's open source program is part of the [Community Relations team](/handbook/marketing/community-relations/). It consists of three sub-programs:

* the GitLab for Open Source Program
* the Open Source Partners Program
* Consortium Memberships

| Program Name & Section in the Handbook | Description | Landing Page or Workboard |
| ------ | ------ | ------ |
| [GitLab for Open Source Program](/handbook/marketing/community-relations/opensource-program/#-gitlab-for-open-source-program) | Qualifying open source projects receive GitLab Ultimate and 50,000 CI minutes for free. We process applications according to the [Community Programs Application Workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/). | [GitLab for Open Source Program Landing Page](/solutions/open-source/) |
| [Open Source Partners Program](/handbook/marketing/community-relations/opensource-program/#-gitlab-open-source-partners) | A partnership program designed for large or prominent open source organizations | [Open Source Partners Program Landing Page](/solutions/open-source/partners) |
| [Consortium Memberships](/handbook/marketing/community-relations/opensource-program/#-consortium-memberships-and-sponsorships) | These memberships allow us to extend GitLab's leadership in key open source initiatives, enhance GitLab's brand, and/or improve engineering alignment | [Consortium Membership Workboard](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships/-/boards) |

# <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to reach us
 * DRI: @bbehr
 * Slack channel: #community-programs
 * Email: opensource@gitlab.com

# <i class="fas fa-tasks" id="biz-tech-icons"></i> What we're working on
We use epics, issue boards, and labels to track our work. We also maintain [quarterly objectives and key results](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=OSS%20OKR).

See the [Community Relations project management page](/handbook/marketing/community-relations/project-management/) for more detail.

# <i class="far fa-newspaper" id="biz-tech-icons"></i> GitLab for Open Source Program

*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.*

The [GitLab for Open Source program](/solutions/open-source/) supports GitLab's mission by empowering and enabling open source projects with our most advanced features so that they can create greater impact and amplify the contribution mindset within their spheres of influence.

This program's vision is to make GitLab the best place for Open Source projects to thrive at scale.

Apart from actively maintaining and constantly adding value to the open source [Community Edition](/install/ce-or-ee/), GitLab makes the Enterprise Edition's top [Ultimate](/pricing/#self-managed) and [Gold](/pricing/#gitlab-com) tiers available free-of-cost to qualifying open source projects.

### What's included?
- GitLab's top tiers ([self-managed Ultimate and cloud-hosted Gold](/pricing/)) are free for open source projects
- 50,000 CI minutes are included for free for GitLab Gold users. Additional CI minutes can be purchased ($8 one-time fee for 1,000 extra CI minutes)

More information about the program can be found on the [GitLab for Open Source landing page](/solutions/open-source/).

Note: This program grants Ultimate features at the _group_ level (these features are things like epics, roadmaps, merge requests, and other Ultimate features for groups).

#### Support, add-ons, and additional services

**Migration Services.**
For open source communities that need assistance with their migration, our team provides a variety of services to help. Check out our [professional services](https://about.gitlab.com/services/) page for more information.

**Additional services.**
We sometimes hear requests for services we do not provide, such as providing hosting for Community Edition instances. We've partnered with others to help expand the options for our users. Check out our [Technology Partners](/partners/technology-partners/) to learn more.

### OSS Product SKUs
The following SKUs are related to the GitLab for Open Source program:

 * [OSS Program] SaaS - Ultimate (formerly Gold) - 1 Year
 * [OSS Program] SaaS - Ultimate (formerly Gold) - 1 Year w/ Support
 * [OSS Program] Self-Managed - Ultimate - 1 Year
 * [OSS Program] Self-Managed - Ultimate - 1 Year w/ Support
 * [OSS Program] Self-Managed - Ultimate w/ Support - 3 Year (not being used right now)

### Who qualifies for the GitLab for Open Source program?

In order to qualify, projects must meet the program requirements listed on the [GitLab for Open Source application page](https://about.gitlab.com/solutions/open-source/join/).

Here are some guidelines that help us make decisions about whether or not projects qualify:

**Examples of OSS projects that usually qualify:**
 * _Receives Donations_ -- The software is being developed by a community that gathers donations for covering costs.
 * _Has an open governance model_ -- Projects with open governance models are likely to be accepted. For more information about open governance models, see this open source governance model article. The only model in there that would NOT qualify is the corporate governance model.

**Examples that usually do not qualify:**
 * _Open Core_ -- The software is being primarily developed by a company that uses the software for its core or base product. The company charges for use of the software at higher tiers.
 * _Charging for Services_ -- The software itself is not being sold but the open source project is being primarily developed by a company that charges for services around that open source project.

For more questions, please see our [GitLab for Open Source FAQ](/solutions/open-source/#FAQ) or contact us at `opensource@gitlab.com`

#### Federal Exception Policy
Unfortunately, we are not able to accept all open source projects that are affiliated with the US Federal government. Projects that are affiliated must work with a Sales representative to see if they qualify.

#### Strategic Qualification Exceptions
From time to time, we make strategic exceptions to our program requirements, such as to the [federal exception policy](/handbook/marketing/community-relations/opensource-program/#federal-exception-policy). These requests must be made by a GitLab Sales team member on behalf of a project.

Requests for qualification exceptions can be made by filing a [new issue](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/issues/new) on the GitLab for Open Source program kanban board and selecting the `oss-qualification-request` template.

In order for this to be approved, both Account Executives and their Managers will need to sign off by checking the box next to their name in the submitted issue. If there is a relevant Technical Account Manager (TAM) associated with the account, they should be added to the issue in FYI so that they are notified of the exception request.

## Application Process and Workflow
Qualifying open source projects can apply for self-managed Ultimate or SaaS Gold by submitting the [GitLab for Open Source application form](/solutions/open-source/join/).

We follow then follow the **[community programs application workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/)** to process applications.

For more information on what applicants can expect, please see the `Application Process` section of the [GitLab for Open Source application form](/solutions/open-source/join/).

## Resources and additional open source programs
There are a number of organizations that offer open source programs with discounts or free services just for open source projects. Here is a list of some of the programs we know that have been useful to pair with the GitLab for Open Source program:

 * [AWS](https://aws.amazon.com/blogs/opensource/aws-promotional-credits-open-source-projects/) - Provides promotional credits for hosting to qualifying open source projects.
 * [Discourse](https://free.discourse.group/) - Provides a zero dollar cloud hosted forum/mailing list replacement for open source projects.

### GitLab Forum & Docs
[GitLab Docs](https://docs.gitlab.com/) are a great resource for our program members since our docs are the single source of truth and answer a wide variety of questions program members may face.

If program members are unable to find what they're looking for in our docs, we ask that they post their question in the [GitLab Forum](https://forum.gitlab.com/). There, they can interact with other members of our community and many GitLab employees. We encourage program members to join the forum even if they don't have questions so that they can meet other members of our community.

### Submitting feedback: bugs, features, and more
In line with our Transparency value, GitLab has an [open roadmap](/direction/), so our community can always see what's ahead in the product priorities.

Feedback helps us to build the best product possible. Whether it's a bug report, a feature request, or feedback to our company, here's a guide for [how to submit feedback](/submit-feedback/).

**Make sure to Thumbs Up issues.**
Our team looks at the number of `Thumbs Up` votes as an indicator for priority, so make sure you're using that feature on issues as a quick way to give us feedback. Writing detailed comments of your use-case and thorough issue desciptions is another way you can help make it easier for our team to understand your needs and respond to your questions.

Just search for relevant issues on the [GitLab product project](https://gitlab.com/gitlab-org/gitlab), and start giving us your feedback!


## Updating the GitLab for Open Source members list

When the GitLab for Open Source program began, we kept track and publicly displayed the members who joined the GitLab for Open Source Program at https://about.gitlab.com/solutions/open-source/projects/. This list is currently deprecated since it's quite out of date. However, we are retaining information about it for historic reference.

To do this, applicants submitted a Merge Request to the [GitLab OSS project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/). Whenever the applicant was approved, and as part of the process, the project was added to a [master list on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml).

**Unfortunately, the [open source project list](/solutions/open-source/projects/) is now outdated because of the following reasons:**
 * The Merge Request step was removed from the application process to simplify the process (by lowering the barrier of entry for applicants), and in order to allow the program to scale.
 * The list became naturally outdated since some projects listed did not renew, so the list is not a reflection of active OSS projects on GitLab today.

This section talks about how we used to update the open source project list so that we preserve this knowledge as we plan for future iterations of an open source project directory.

While the original Merge Request process was ideally meant to keep the list updated, there was also a manual step involved in transferring the file from the `/gitlab-com/marketing/community-relations/gitlab-oss` repository to the GitLab's website repository.

These additional steps were required to update the GitLab for Open Source members list on the website:

1. Fetch the [latest list of GitLab for Open Source members on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/-/raw/master/data/oss_projects.yml)
1. Copy the contents of that file into the [GitLab website's `data/oss_projects.yml` file](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/blob/master/-/data/oss_projects.yml)
1. Commit the changes and submit an MR to merge the changes into the website's master branch
1. Once the MR has been merged, the GitLab for Open Source members list will be up to date at https://about.gitlab.com/solutions/open-source/projects

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> At this time, the [master list on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml) is only updated for applications, not for renewals. As such, it might contain data from projects who initially applied for the program but did not renew their yearly license.
{: .alert .alert-warning}

# <i class="far fa-newspaper" id="biz-tech-icons"></i> GitLab Open Source Partners
**Purpose:** The GitLab Open Source Partners program seeks to build relationships with open source projects that have thousands of community members or users.

Through our partnership, we aim to gain insight to help us build a better product and navigate the challenges of being an open core, for-profit company. We also aim to create greater outreach through co-marketing and special initiatives.

**About:** Full program benefits and requirements can be found on the [GitLab Open Source Partners landing page](/solutions/open-source/partners/).

We have Open Source partners that use a variety of GitLab's products. In addition to Ultimate SaaS or Self-Managed, some of our Open Source Partners use the [Community Edition](https://about.gitlab.com/install/ce-or-ee/) instead of the Enterprise Edition because of their strong FOSS values.

## Onboarding OSS Partners

### Email templates
We have a number of [email templates](https://docs.google.com/document/d/1et0t3CLQdT8I2UbxBVrpFqMXmnd6cEtMZuE61V1DiXM/edit#) to help us interact with prospective and current OSS Partners. These email templates are publicly visible to allow our community to learn from our onboarding process.

### Onboarding Packet

When an org has joined the OSS Partners program, we send them the [OSS Partners Onboarding Packet](https://docs.google.com/presentation/d/1VvRza5K_fAYTXVfxGF494cQM7vIUxSv7vPMTtmo-PmY/edit#slide=id.gd40831fb97_0_0).

### Contact Information

We need to make sure that we have the right contact information for our Open Source Partners.

Suggested touchpoints for requesting contact information and updates:
 * Onboarding new OSS Partners (within first 30 days) -- make sure initial contacts are given.
 * Once a year (during Q4) request a refresh

Some OSS Partners will not have contacts for each function. If that's the case, the contact for that function should be left blank and the Primary contact should be contacted instead.

Multiple contacts can be added for each of these functions except the Primary contact.

#### Contact functions
 * **Primary:** Represents OSS Partner at meetings and is our main line of contact. Main relationship owner.
 * **Alternate:** Will be contacted if we are unable to reach the Primary contact and will be the backup representative.
 * **Marketing:** Contacted when event and marketing opportunities for OSS Partners arise.
 * **Technical:** Contacted for participation in surveys or focus studies that require technical expertise, or when something at GitLab may require input from technical contacts from our OSS Partner orgs. Usually someone from the SysAdmin team is listed as the Technical contact.
 * **Legal:** (optional) The person or team who can weigh in on legal matters like updates to terms of service agreements or partnership activities.
 * **Others:** Anyone else who is a key stakeholder to the OSS Partner org. Please specify their role and when to contact.

**Related Documents:**
 * **[OSS Partners Contact Info](https://docs.google.com/spreadsheets/d/1UFaRATA8I2mmcZ-77KBXMoNZCmut5TalZytIzWCh1HQ/edit#gid=484404042)** -- This is confidential, internal information and requires a GitLab team member login. OSS Partners can request updates or removal of information at any time.
 * **[Email Templates](https://docs.google.com/document/d/1et0t3CLQdT8I2UbxBVrpFqMXmnd6cEtMZuE61V1DiXM/edit#heading=h.jc6mgcides78)** -- This document contains email templates that are useful for asking for contact information at the suggested times.

### Public Issue Tracker
We use [public issue trackers](https://gitlab.com/gitlab-org/gitlab/-/boards/1625116?&label_name[]=Open%20Source%20Partners) to help our [OSS Partners](/solutions/open-source/partners/) through and beyond migration.

Public issue trackers help us understand the priority of various feature requests and bugs for our Open Source Partners (priority is denoted as: blockers, urgent, important, and nice-to-have). When OSS Partners set up a public issue tracker, it has the added benefit of helping them get used to our workflow so that they can start giving us more product feedback and helping us improve the product together.

We ask that a representative of the OSS Partner org opens the issue tracker so that they are able to edit the description section. If someone at GitLab opens the issue on the OSS Partner's behalf, the OSS Partner will not have edit access.

Follow these steps to create a public issue tracker:

 1. Create a [New Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new) in the `gitlab.com/gitlab-org/gitlab` [project](https://gitlab.com/gitlab-org/gitlab/)
 1. Select the `OSS_Partner` template where it prompts you to select a template, and click on `Apply template`
 1. Add in as much information as possible. Once finished, submit it.

 Examples of public issue trackers:
  * [KDE](https://gitlab.com/gitlab-org/gitlab/-/issues/24900)
  * [Eclipse Foundation](https://gitlab.com/gitlab-org/gitlab/-/issues/195865)
  * [Freedesktop](https://gitlab.com/gitlab-org/gitlab/-/issues/217107)


## Project Management for OSS Partners

We use the [GitLab Open Source Partners project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/oss-partners-program/-/boards) to keep internal notes around collaboration with our OSS Partners. This project is accissible only by GitLab team members only as it contains sensitive information that should not be made publicly available. This board also contains a view of all Open Source Partners and has labels to help us identify if they align with any of our GTM messaging.

We use the GitLab Open Source Partners project to track the main issues for the partnership. Issues relating to strategic initiatives with our OSS Parrtner are added to the [`general` OSS program board](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/boards/1755105) and are linked back to the relevant main OSS Partner issue.

More information about the project management for this program can be found on the main [Community Relations project management page](https://about.gitlab.com/handbook/marketing/community-relations/project-management/).

### Linux Foundation Partnership
We are exploring ways to partner with the Linux Foundation to make it easier for Linux Foundation member projects to apply for and qualify for the GitLab for Open Source program. We are using the [GitLab <> LF project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/linux-foundation) to collaborate with their team on various initiatives.

The application process for LF members projects [can be found here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94599).FIXME

Note: In addition to having the Linux Foundation as an Open Source Partner, we are a paying member of the Linux Foundation through their consortium membership program. More information about this consortium membership can be found [in the section below](/handbook/marketing/community-relations/opensource-program/#current-memberships).

## Editorial plan for OSS Partners
The Open Source Program Manager worked with the Content team on an editorial strategy to meet the needs of the Open Source Partners program. Posts about technical content and open source partner migrations tend to be very popular, so the Content team collaborates by brainstorming topics and doing reviews.

The plan is to publish posts on the [GitLab Blog](/blog/) that are related to the Open Source Partner program and to feature these on the [Open Source Partners page](/solutions/open-source/partners/).

Marketing opportunities:
 * Migration announcement
 * Migration phase complete
 * Case Study
 * Technical challenge and resolution, use case that aligns with other marketing campaigns such as CI/CD, security, or project management

Resources:
 * [Template - OSS Partner Use Case Blog Posts](https://docs.google.com/document/d/1oRwrwoo6PkuqafAsde11mqCnMvAI2KNZ9E7AfnGg9Fw/edit#) - includes template blog post questions to ask OSS Partners in order to publish posts about migrations, CI/CD, Security, and Project Management use cases.
 * [OSS Blog Posts and Views](https://docs.google.com/spreadsheets/d/1LpgSudtcgXDkPb7XX-4s2PW6vYAQZpDgQRdGw7OWqBY/edit#gid=0)

## How to add a new logo to the Open Source Partners page
Our Open Source Partners are shown on the [Open Source Partners](/solutions/open-source/partners) page and the main [GitLab for Open Source program](/solutions/open-source/) page. These are the steps to add a new logo to those sections.

### 1. Add a new org to the Organizations list
You'll need to add an entry to the [`data/organizations.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/organizations.yml) file using GitLab's WebIDE.

Follow this format (you can copy and paste it at the bottom of the organizations.yml file):

```markdown
- name: ORG NAME
  logo: /images/organizations/logo_ORG NAME.svg
  logo_color: /images/organizations/logo_ORG NAME_color.svg  
  industry_type: 'open source software'
  home_url:  
  landing: false
  opensource_partner: true
```

Example:

```markdown
- name: Inkscape
  logo: /images/organizations/logo_inkscape.svg
  logo_color: /images/organizations/logo_inkscape_color.svg  
  industry_type: 'open source software'
  home_url: https://inkscape.org
  landing: false
  opensource_partner: true
```

### 2. Prep the logos
#### Create two SVGs: one color, one grey
 1. **Get an SVG.** Make sure you have an SVG file. If you weren't provided with one, look for a vector (`*.svg`) version of the logo in the official channels (e.g. the [ARM trademarks page](https://www.arm.com/company/policies/trademarks/guidelines-corporate-logo) has SVG as one of the download options). If you can't find the SVG file on their channels, you may need to Google it and find one on Wikimedia Commons. Download the original file.
 1. **Duplicate SVG and name each copy.** Duplicate the file and name each of the two files as follows:
     * `logo_ORG Name.svg` -- Example: `logo_inkscape.svg`. This will be the greyscale image that will be shown on the customer reference page.
     * `logo_ORG Name_color.svg` -- Example: `logo_inkscape_color.svg`. This will be the color image that displays on the Open Source Partners page.
 1. **Create greyscale logo.** These instructions are specifically for using [Inkscape](https://inkscape.org/) as the image editor, which you can download for free for most operating systems, including Mac.
     * Open the SVG image with Inkscape.
     * In the top menu, go to `Filters` -> `Color` -> `Greyscale`. Click on `Live Preview` so you can see what will happen. Save your changes.
     * Export the logo and make sure that it is called: `logo_ORG Name.svg` as detailed above.

Note: If you are not using Inkscape, aim for getting a greyscale image with Hex Color Code: Dark Grey `#444444` and then export the greyscale logo as an SVG with a transparent background.

#### Optimize the SVGs
Do this for both color and grey logos:
 1. Go to this SVG optimization website: https://jakearchibald.github.io/svgomg/ and upload one of the logos
 1. Make sure that the `Prefer viewBox to width/height` factor is toggled `On`. It should be near the bottom of the list of optimization factors.
 1. Export the SVG and save

### 3. Upload the logo via WebIDE
 * Go to `source/images/organizations` and upload the two prepped logos

Once you've completed these steps, check to make sure that the pipleline works, and check out the preview. If it looks ok, have someone review and merge!

### Review the changes
Unfortunately, the review app will not automatically show you the preview. Here's how to generate a link to preview the changes.

#### Why does the View App not point to the correct webpage?

The View App lets you preview your changes. It detects which files have been changed, and then creates a URL, or a set of URLs, pointing to the built pages with the changes.

Since this type of merge request (adding a new logo to the OSS partners page), does not change any HTML files, the View App does not know where to point to. It defaults to the GitLab home page.

#### How do I get the review app to show the correct page?

Since the `View App` points to the GitLab home page for this build as mentioned above, we need to add the path to the webpage you'd like to preview.

To do this, you need to be familiar with four parts of the View App URL:
1. **The beginning of the URL:** `https://` -- it doesn't include "www"
1. **Your branch name:** You can copy this from your Merge Request (MR) in the "Request to Merge" section directly under the MR description.
1. **The View App snippet:** `.about.gitlab-review.app` -- this is what builds the preview
1. **The path to the webpage you'd like to preview:** This is everything after "about.gitlab.com/" on the webpage you're editing

Example:
1. `https://`
1. `c_hupy-master-patch-24835` (branch name)
1. `.about.gitlab-review.app`
1. `/solutions/open-source/partners` (path to the webpage I want to preview)

This formula creates: `https://c_hupy-master-patch-24835.about.gitlab-review.app/solutions/open-source/partners`. This link allows you to preview the MR changes for the `c_hupy-master-patch-24835` branch on the GitLab Open Source Partners page.

For ease of use, here's the formula:
```
`https://` [your branch] `.about.gitlab-review.app` [rest of the path to the page you want to build]
```

You should now be able to create preview links when the View App doesn't work!


# <i class="far fa-newspaper" id="biz-tech-icons"></i> Consortium Memberships and Sponsorships

GitLab's open source program also oversees GitLab's representation and participation in select industry consortia.

## FAQs

### What is a consortium?
We define "consortium" as a group createdto further some technological cause. In the context of open source software, a prototypical consortium would be the [Linux Foundation (LF)](https://en.wikipedia.org/wiki/Linux_Foundation), a non-profit organization founded in 2000 as a merger between Open Source Development Labs and the Free Standards Group, which [hosts and promotes](https://www.linuxfoundation.org/about/) collaborative development of open source software projects.

### Why is consortium marketing important?
Consortia are influential leaders in their respective ecosystems, as they often host conferences and underwrite programs that influence global conversations about particular technological developments. Participating in consortia enhances GitLab's brand—and helps align GitLab's engineering efforts with global efforts and trends.

### How does GitLab particpate in consortium activities?
While select consortium memberships fall within the purview (and budget) of GitLab's open source program, the [Developer Evangelism team](/handbook/marketing/community-relations/developer-evangelism/) focuses on consortium marketing, working to integrate GitLab's overall community message and technical perspective into the most appropriate and effective industry conversations.

### How can I recommend GitLab get involved in a consortium?
You can open an issue in the [Consortium Memberships project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships). When you do, please use the `membership-evaluation` template to structure your issue. Open source program team members will evaluate your application using the following criteria. When we review the application, we'll assess it with these considerations in mind:

* Awareness opportunities
* Ease of collaboration
* Contribution and hiring pool

| Considerations | What we're interested in | Questions we're asking |
| --------------| ------------------ | -----------------------------|
| Awareness opportunities | Size of the organization<br /><br />Frequency and impact of marketing opportunities<br /><br />| How many authenticated and non-authenticated users are visiting organization's website monthly?<br /><br />How many people are part of the organization’s community?<br /><br />What sorts of marketing and communication channels (social media platforms, newsletters, blogs, events) does the organization use?<br /><br /> Will GitLab appear in those official channels? How prominent would our placement be? | 
| Ease of collaboration | Access to a dedicated marketing resources/point person<br /><br />Time-to-execute for standard communication types | Does the organization have marketing capacity?<br /><br />How mature is the organization's brand and marketing portions?<br /><br />How quickly can this organization produce a resource (e.g., a case study)? A week? A month? A quarter?<br /><br />How responsive is the person in charge of the relationship?<br /><br />Is marketing handled by volunteers or paid employees? |
| Contribution and hiring pool | Size of contributor/member base<br /><br />Overall community/member activity<br /><br />Frequency of community contribution<br /><br />Rate of adoption | How active is the community the organization is attempting to foster?<br /><br /> Does the organization have a sense of its community's health?<br /><br />Do we see hiring opportunities opportunities to recruit from the community's talent pool?<br /><br />What is the growth of the community or foundation itself?<br /><br />Do we see job opportunities within that software ecosystem (are people hiring contributors from this community in general)?<br /><br />How can GitLab contribute in ways that align with our interests?<br /><br />Can GitLab participate in the project's roadmap in ways that creates mutual value? |

### In which consortia is GitLab involved?
We are currently members of the following consortia:

* [Continuous Delivery Foundation](https://cd.foundation/)
* [Cloud Native Computing Foundation](https://www.cncf.io/)
* [Fintech Open Source Foundation](https://www.finos.org/)
* [InnerSource Commons](https://innersourcecommons.org/)
* [Linux Foundation](https://www.linuxfoundation.org/)
* [Open Source Security Foundation](https://openssf.org/)
* [TODO Group](https://todogroup.org/)

[Complete details of GitLab's activities](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships) with these groups are available in the `Consortium Memberships` project. Note that because this project contains sensitive data and personally identifying information, it is only accessible to GitLab team members.

## Elections for Board of Directors opportunities
Some of the consortia in which we participate allow members to run for their respective Boards of Directors. Anyone interested in becoming more involved in any of the consortia GitLab supports should visit the `Consortium Memberships` [project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships) and open an issue.

Review the information below if you're thinking of seeking nomination for (or election to) consortium positions.

### Internal nominations
Create an issue for each nomination so GitLab team members who are also interested in running can discuss, and so additional internal nominations can occur. Community Relations, Alliances, and Marketing leadership (CMO, Director of Corporate Marketing) teams will likely be involved. Prepare a [nomination statement](https://docs.google.com/document/d/1IrtEBGfjuwi8Tz87U0vOsCxN9-f2-Krt1t-db87nyzU/edit?ts=60c13066#heading=h.5tbh3ex4b8al) that explains your interest in joining the board. Post this as part of your issue so our internal teams can help you polish it.

### Campaigning
Once GitLab candidates are nominated, the Community Relations team can help them campaign for their positions. We'll make other GitLab team members  aware of the election and equip them to assist your campaign, too (e.g., by announcing the campaign on the `#whats-happening-at-gitlab` Slack channel).

### Promoting
The social media team is able to promote elections notification news. They simply need a place to point people, preferably an updated webpage that lists the board of directors or a social media post from the organization that mentions the election results.

## Additional sponsorship types
We have a small budget to sponsor events that allow us to engage with and build relationships among our current open source partners' communities. All other event sponsorship requests, are handled by our [field marketing team](/handbook/marketing/field-marketing/#3rd-party-events).  

### Open Source Sponsorship Resources
**We use this [adapted boilerplate copy](https://about.gitlab.com/handbook/marketing/strategic-marketing/messaging/#gitlab-value-proposition) to describe GitLab at open source conferences. If you are an event organizer we are working with, please feel free to use this to add GitLab to your site and materials.**

Description:

`[GitLab](https://about.gitlab.com/solutions/open-source/)` is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity.

Built on Open Source, GitLab works alongside its community of thousands of contributors to continuously deliver new DevOps innovations. From startups to global enterprises to leading open source projects, over 30 million users across the globe trust GitLab to deliver great software at new speeds.

**Logo:**

You can find png, jpg, svg, and eps files here: https://about.gitlab.com/press/press-kit/

We tend to use the `Logo - gray` and `Stacked Logo - gray` logos most, but any of these should work.

## Concepts and membership best practices

### Seat usage

1. Seats are counted differently between Self-Managed (SM) instances and SaaS:
    1. SM counts seats at the instance level: every active account on a GitLab EE instance counts toward the license.
    1. SaaS subscriptions counts seats at the group level: only users added to a group or subgroup take up a seat that counts toward the license.
1. There are no seat limitations on SM instances running the GitLab CE distribution.

# <i class="far fa-newspaper" id="biz-tech-icons"></i> Measuring our success

Our team measures the success of our work in the following ways.

## Sisene
We use the [GitLab for Open Source KPI Dashboard](https://app.periscopedata.com/app/gitlab/670411/Open-Source-Program) on Sisene to track program metrics. This dashboard's visibility is internal to GitLab team members only and measures the following:

**Current status stats:**
These stats give us an idea of how many active users we have as part of the GitLab for Open Source program.
 * **Active projects** - number of program members that have an active new application or renewal.
 * **Active seats** - number of users under the GitLab for Open Source program. Since we do not enable Telemetry, this number is taken from the number of seats program members request.

**Stats counted from program inception:**
Lifetime stats of how the program has performed since its creation.
 * **Number of licenses** - these are licenses issued since the beginning of the program (includes add-ons, and renewals).
 * **Enrolled projects** - how many unique projects have enrolled since the program began.
 * **Projects renewed** - the percentage of projects that have renewed since program began.
 * **Seats issued** - total seats issued since beginning of program.

**Quarterly stats:**
 * New projects enrolled per quarter
 * Number of seats granted per quarter

## Impressions Dashboard
We keep track of OSS impressions in the [OSS Impressions Dashboard](https://datastudio.google.com/u/0/reporting/b62b70f4-ed1d-4f0a-8ed4-ebbcbca29962/page/YsgmB).

This dashboard takes into account:

 * Blogs
 * YouTube
 * Social media impressions
 * Media (PR, placed articles)
 * Events

The dashboard is generated by the content in:
 * [OSS Marketing Spreadsheet](https://docs.google.com/spreadsheets/d/1LpgSudtcgXDkPb7XX-4s2PW6vYAQZpDgQRdGw7OWqBY/edit#gid=0) -- Blogs, YouTube, Social Media, Media
 * [OSS Marketing Board](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/oss-marketing/-/boards) -- Events

## Cumulative Data
We only use cumulative metrics in external presentations as per our [communication guidelines (#8 of presentation section)](https://about.gitlab.com/handbook/communication/#presentations).

OSS cumulative metrics are kept in a spreadsheet: [OSS Cumulative Metrics](https://docs.google.com/spreadsheets/d/1wr1G7N9-XiQmfkIcq3wNvS3JE0-EdOOAkqpG5TvkW6o/edit#gid=1969501712). They include the number of new projects and new seats per fiscal quarter.

To generate these cumulative graphs:
1. Go to the [OSS dashboard on Sisene](https://app.periscopedata.com/app/gitlab/670411/Open-Source-Program).  
1. Download `New projects enrolled per fiscal quarter` and `Number of new seats issued per fiscal quarter` by clicking on the hamburger menu by each and clicking on **Download Data**.
1. Go to the [OSS Cumulative Metrics](https://docs.google.com/spreadsheets/d/1wr1G7N9-XiQmfkIcq3wNvS3JE0-EdOOAkqpG5TvkW6o/edit#gid=1969501712) sheet and copy any existing tab for 'New Projects` and `New Seats`. Paste the data from the relevant Sisene downloads there. This will generate new graphs that you can use to display cumulative metrics per fiscal quarter.

# Website Management

The [GitLab for Open Source program webpage](/solutions/open-source/) and all of the child pages can be modified using Netlify CMS. Instructions for how to access and edit them can be [found here](/handbook/marketing/netlifycms/#accessing-the-admin). To add new OSS Partners, you'll still need to follow the [process detailed below](/handbook/marketing/community-relations/opensource-program/#how-to-add-a-new-logo-to-the-open-source-partners-page).
